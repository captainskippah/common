<?php

namespace Captainskippah\Common\Event;

use Captainskippah\Common\Domain\DomainEvent;

final class EventStream
{
    /**
     * @var DomainEvent[]
     */
    private $events;

    /**
     * @var int
     */
    private $version;

    public function __construct(array $events, int $version)
    {
        $this->events = $events;
        $this->version = $version;
    }

    /**
     * @return DomainEvent[]
     */
    public function events(): array
    {
        return $this->events;
    }

    /**
     * @return int
     */
    public function version(): int
    {
        return $this->version;
    }
}
