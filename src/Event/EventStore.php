<?php

namespace Captainskippah\Common\Event;

use Captainskippah\Common\Domain\AbstractId;
use Captainskippah\Common\Domain\DomainEvent;

interface EventStore
{
    /**
     * @param AbstractId $id
     * @return EventStream
     */
    public function loadEventStream(AbstractId $id): EventStream;

    public function appendToStream(AbstractId $id, int $streamVersion, DomainEvent ...$events);

    public function deleteStream(AbstractId $id);
}
