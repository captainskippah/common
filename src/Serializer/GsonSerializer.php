<?php

namespace Captainskippah\Common\Serializer;

use Tebru\Gson\Gson;

class GsonSerializer implements Serializer
{
    /**
     * @var Gson
     */
    private $gson;

    public function __construct(Gson $gson)
    {
        $this->gson = $gson;
    }

    public function serialize($object): string
    {
        return $this->gson->toJson($object);
    }

    public function deserialize(string $serializedData, string $type)
    {
        return $this->gson->fromJson($serializedData, $type);
    }
}
