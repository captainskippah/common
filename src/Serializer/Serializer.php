<?php

namespace Captainskippah\Common\Serializer;

interface Serializer
{
    public function serialize($object): string;

    public function deserialize(string $serializedData, string $type);
}
