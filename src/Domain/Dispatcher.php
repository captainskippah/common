<?php

namespace Captainskippah\Common\Domain;

class Dispatcher
{
    /**
     * @var self
     */
    private static $instance;

    /**
     * @var EventListener[]
     */
    private $listeners = [];

    public static function instance(): self
    {
        if (!isset(self::$instance)) {
            self::$instance = new self();
        }

        return self::$instance;
    }

    public static function destroy()
    {
        self::$instance = null;
    }

    public function register(EventListener $eventListener)
    {
        $this->listeners[] = $eventListener;
    }

    public function dispatch(DomainEvent $event)
    {
        foreach ($this->listeners as $listener) {
            $listener->handle($event);
        }
    }
}
