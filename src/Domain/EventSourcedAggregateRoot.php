<?php

namespace Captainskippah\Common\Domain;

use Captainskippah\Common\Event\EventStream;

abstract class EventSourcedAggregateRoot extends AggregateRoot
{
    /**
     * @var int
     */
    protected $unmutatedVersion = 0;

    public static function fromEventStream(EventStream $eventStream)
    {
        $reflectionClass = new \ReflectionClass(get_called_class());

        $entity = $reflectionClass->newInstanceWithoutConstructor();

        foreach ($eventStream->events() as $event) {
            $entity->when($event);
        }

        $property = $reflectionClass->getProperty('unmutatedVersion');
        $property->setAccessible(true);
        $property->setValue($entity, $eventStream->version());

        return $entity;
    }

    public function unmutatedVersion(): int
    {
        return $this->unmutatedVersion;
    }

    protected function apply(DomainEvent $event)
    {
        $this->events[] = $event;

        $this->when($event);
    }

    protected function when(DomainEvent $event)
    {
        $method = 'when' . $event::eventName();

        if (!method_exists($this, $method)) {
            throw new \InvalidArgumentException($event::eventName() . ' is not supported by this aggregate');
        }

        $this->$method($event);
    }
}
