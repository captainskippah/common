<?php

namespace Captainskippah\Common\Domain;

abstract class AbstractId
{
    /**
     * @var string
     */
    private $id;

    protected function __construct(string $id)
    {
        if (empty($id)) {
            throw new \InvalidArgumentException('ID cannot be empty');
        }

        $this->id = $id;
    }

    public function value(): string
    {
        return $this->id;
    }

    public function equals(self $other)
    {
        return $this->value() === $other->value();
    }
}
