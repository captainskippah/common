<?php

namespace Captainskippah\Common\Domain;

abstract class EventListener
{
    final public function handle(DomainEvent $event)
    {
        if (static::canHandle($event)) {
            $this->doHandle($event);
        }
    }

    abstract public static function canHandle(DomainEvent $event): bool;

    abstract protected function doHandle(DomainEvent $event);
}
