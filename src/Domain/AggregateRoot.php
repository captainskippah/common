<?php

namespace Captainskippah\Common\Domain;

abstract class AggregateRoot
{
    /**
     * @var DomainEvent[]
     */
    protected $events = [];

    public function events()
    {
        return $this->events;
    }

    public function clearEvents()
    {
        $this->events = [];
    }
}
