<?php

namespace Captainskippah\Common\Domain;

use Carbon\CarbonImmutable;
use DateTimeImmutable;

abstract class DomainEvent
{
    /**
     * @var DateTimeImmutable
     */
    private $occurredOn;

    protected function __construct()
    {
        $this->occurredOn = new CarbonImmutable();
    }

    public function occurredOn(): CarbonImmutable
    {
        return $this->occurredOn;
    }

    final public static function eventName(): string
    {
        return (new \ReflectionClass(get_called_class()))->getShortName();
    }
}
