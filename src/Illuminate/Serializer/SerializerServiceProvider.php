<?php

namespace Captainskippah\Common\Illuminate\Serializer;

use Captainskippah\Common\Serializer\GsonSerializer;
use Captainskippah\Common\Serializer\Serializer;
use Doctrine\Common\Annotations\AnnotationRegistry;
use Illuminate\Support\ServiceProvider;
use Tebru\Gson\Gson;

class SerializerServiceProvider extends ServiceProvider
{
    public function register()
    {
        AnnotationRegistry::registerLoader('class_exists');

        $this->app->singleton(Serializer::class, function ($app) {
            $builder = Gson::builder();

            $builder->enableCache(true)
                ->setCacheDir(storage_path('cache/serializer'))
                ->setDateTimeFormat(\DateTime::ATOM);

            $gson = $builder->build();

            return new GsonSerializer($gson);
        });
    }
}
