<?php

namespace Captainskippah\Common\Illuminate\Event;

use Captainskippah\Common\Domain\AbstractId;
use Captainskippah\Common\Domain\Dispatcher;
use Captainskippah\Common\Domain\DomainEvent;
use Captainskippah\Common\Event\EventStore;
use Captainskippah\Common\Event\EventStoreException;
use Captainskippah\Common\Event\EventStream;
use Captainskippah\Common\Serializer\Serializer;
use Illuminate\Database\Connection;

class DBEventStore implements EventStore
{
    /**
     * @var Connection
     */
    private $database;

    /**
     * @var Serializer
     */
    private $serializer;

    public function __construct(Connection $database, Serializer $serializer)
    {
        $this->database = $database;
        $this->serializer = $serializer;
    }

    public function loadEventStream(AbstractId $id): EventStream
    {
        $queryResult = $this->query()
            ->select([
                'body',
                'type',
                'version'
            ])
            ->where([
                'aggregate_id' => $id->value()
            ])
            ->get();

        $events = [];
        $version = 0;

        $queryResult->each(function (object $queryResult) use (&$events, &$version) {
            $events[] = $this->serializer->deserialize($queryResult->body, $queryResult->type);
            $version = $queryResult->version;
        });

        return new EventStream($events, $version);
    }

    public function appendToStream(AbstractId $id, int $streamVersion, DomainEvent ...$events)
    {
        if (empty($events)) {
            return;
        }

        $values = [];

        foreach ($events as $index => $event) {
            $values[] = [
                'body' => $this->serializer->serialize($event),
                'type' => get_class($event),
                'aggregate_id' => $id->value(),
                'version' => $index + $streamVersion
            ];
        }

        try {
            $this->query()->insert($values);

            $this->dispatchEvents(...$events);
        } catch (\Exception $exception) {
            throw new EventStoreException($exception->getMessage(), null, $exception);
        }
    }

    public function deleteStream(AbstractId $id)
    {
        $this->query()->where('aggregate_id', $id->value())->delete();
    }

    private function query()
    {
        return $this->database->table('event_store');
    }

    private function dispatchEvents(DomainEvent ...$events)
    {
        foreach ($events as $event) {
            Dispatcher::instance()->dispatch($event);
        }
    }
}
