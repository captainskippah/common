<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateEventStoreTable extends Migration
{
    public function up()
    {
        Schema::create('event_store', function (Blueprint $table) {
            $table->increments('id');
            $table->json('body');
            $table->string('type');
            $table->string('aggregate_id');
            $table->integer('version');

            $table->index('aggregate_id');
            $table->unique(['aggregate_id', 'version']);
        });
    }

    public function down()
    {
        Schema::dropIfExists('event_store');
    }
}
