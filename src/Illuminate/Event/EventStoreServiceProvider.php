<?php

namespace Captainskippah\Common\Illuminate\Event;

use Captainskippah\Common\Event\EventStore;
use Captainskippah\Common\Serializer\Serializer;
use Illuminate\Support\ServiceProvider;

class EventStoreServiceProvider extends ServiceProvider
{
    public function register()
    {
        $this->app->bind(EventStore::class, function ($app) {
            return new DBEventStore(
                $app->make('db.connection'),
                $app->make(Serializer::class)
            );
        });
    }

    public function boot()
    {
        $this->publishes([__DIR__ . '/migrations' => database_path('migrations')], 'migrations');
    }
}
