<?php

namespace Captainskippah\Common\Illuminate\Repository;

use Illuminate\Database\Connection;

abstract class AbstractDBRepository
{
    /**
     * @var Connection
     */
    protected $database;

    public function __construct(Connection $database)
    {
        $this->database = $database;
    }
}
