<?php

namespace Captainskippah\Common\Tests\Fixtures;

use Captainskippah\Common\Domain\AggregateRoot;
use Captainskippah\Common\Domain\DomainEvent;

class FakeAggregateRoot extends AggregateRoot
{
    /**
     * @var FakeAggregateId
     */
    private $id;

    public function __construct(FakeAggregateId $id)
    {
        $this->id = $id;
    }

    /**
     * @return DomainEvent[]
     */
    public function events(): array
    {
        return $this->events;
    }

    public function id(): FakeAggregateId
    {
        return $this->id;
    }
}
