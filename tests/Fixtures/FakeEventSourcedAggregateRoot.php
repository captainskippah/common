<?php

namespace Captainskippah\Common\Tests\Fixtures;

use Captainskippah\Common\Domain\EventSourcedAggregateRoot;

class FakeEventSourcedAggregateRoot extends EventSourcedAggregateRoot
{
    private $fakeEventsApplied = [];

    public function addEvent(FakeEvent $event)
    {
        $this->apply($event);
    }

    public function fakeEventsApplied(): array
    {
        return $this->fakeEventsApplied;
    }

    protected function whenFakeEvent(FakeEvent $event)
    {
        $this->fakeEventsApplied[] = $event;
    }
}
