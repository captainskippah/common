<?php

namespace Captainskippah\Common\Tests\Fixtures;

use Captainskippah\Common\Domain\DomainEvent;
use Captainskippah\Common\Domain\EventListener;

class FakeEventListener extends EventListener
{
    private $triggered = false;

    public static function canHandle(DomainEvent $event): bool
    {
        return $event instanceof FakeEvent;
    }

    public function isTriggered()
    {
        return $this->triggered;
    }

    protected function doHandle(DomainEvent $event)
    {
        $this->triggered = true;
    }
}
