<?php

namespace Captainskippah\Common\Tests\Fixtures;

use Captainskippah\Common\Domain\DomainEvent;

class FakeEvent extends DomainEvent
{
    public $payload;

    public function __construct($payload = null)
    {
        $this->payload = $payload;
    }
}
