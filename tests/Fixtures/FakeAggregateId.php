<?php

namespace Captainskippah\Common\Tests\Fixtures;

use Captainskippah\Common\Domain\AbstractId;

class FakeAggregateId extends AbstractId
{
    public function __construct(string $id)
    {
        parent::__construct($id);
    }
}
