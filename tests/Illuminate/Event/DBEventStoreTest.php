<?php

namespace Captainskippah\Common\Tests\Illuminate\Event;

use Captainskippah\Common\Domain\Dispatcher;
use Captainskippah\Common\Event\EventStoreException;
use Captainskippah\Common\Illuminate\Event\DBEventStore;
use Captainskippah\Common\Illuminate\Serializer\SerializerServiceProvider;
use Captainskippah\Common\Serializer\Serializer;
use Captainskippah\Common\Tests\Fixtures\FakeEvent;
use Captainskippah\Common\Tests\Fixtures\FakeEventId;
use Captainskippah\Common\Tests\Fixtures\FakeEventListener;
use Orchestra\Testbench\TestCase;
use Ramsey\Uuid\Uuid;

class DBEventStoreTest extends TestCase
{
    /**
     * @var DBEventStore
     */
    private $eventStore;

    protected function getApplicationProviders($app)
    {
        return array_merge(parent::getApplicationProviders($app), [
            SerializerServiceProvider::class
        ]);
    }

    protected function setUp(): void
    {
        parent::setUp();

        $this->loadMigrationsFrom($this->getEventMigrationsPath());

        $this->eventStore = new DBEventStore(
            $this->getConnection(),
            $this->app->make(Serializer::class)
        );
    }

    public function testShouldAppendToEventStream()
    {
        // Arrange
        $events = [];

        for ($i = 0; $i < 3; $i++) {
            $events[$i] = new FakeEvent("payload {$i}");
        }

        $id = new FakeEventId(Uuid::uuid4()->serialize());

        // Act
        $this->eventStore->appendToStream($id, 1, ...$events);

        // Assert
        $eventStream = $this->eventStore->loadEventStream($id);

        self::assertCount(3, $eventStream->events());
        self::assertEquals('payload 0', $eventStream->events()[0]->payload);
        self::assertEquals('payload 1', $eventStream->events()[1]->payload);
        self::assertEquals('payload 2', $eventStream->events()[2]->payload);
    }

    public function testShouldFailWhenAppendingWrongVersion()
    {
        // Arrange
        $events = [];

        for ($i = 0; $i < 3; $i++) {
            $events[$i] = new FakeEvent("payload {$i}");
        }

        $id = new FakeEventId(Uuid::uuid4()->serialize());

        $this->eventStore->appendToStream($id, 1, ...$events);

        // Act
        try {
            $this->eventStore->appendToStream($id, 1, new FakeEvent("another payload"));

            self::fail("Should have thrown exception");
        } catch (EventStoreException $exception) {
            $this->eventStore->appendToStream($id, 4, new FakeEvent("another payload 2"));

            self::assertTrue(true);
        }
    }

    public function testShouldDeleteStream()
    {
        // Arrange
        $id = new FakeEventId(Uuid::uuid4()->serialize());

        $this->eventStore->appendToStream($id, 1, new FakeEvent('should be deleted'));

        // Act
        $this->eventStore->deleteStream($id);

        // Assert
        $eventStream = $this->eventStore->loadEventStream($id);

        self::assertEquals(0, $eventStream->version());
        self::assertCount(0, $eventStream->events());
    }

    public function testShouldDispatchEvents()
    {
        // Arrange
        $id = new FakeEventId(Uuid::uuid4()->serialize());

        $listener = new FakeEventListener();

        self::assertFalse($listener->isTriggered());

        Dispatcher::instance()->register($listener);

        // Act
        $this->eventStore->appendToStream($id, 1, new FakeEvent('should be deleted'));

        // Assert
        self::assertTrue($listener->isTriggered());
    }

    private function getEventMigrationsPath()
    {
        $reflection = new \ReflectionClass(\Composer\Autoload\ClassLoader::class);

        $basePath = dirname($reflection->getFileName(), 3);

        return $basePath . '/src/Illuminate/Event/migrations';
    }
}
