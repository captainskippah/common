<?php

namespace Captainskippah\Common\Tests;

use Captainskippah\Common\Domain\Dispatcher;
use Captainskippah\Common\Tests\Fixtures\FakeEvent;
use Captainskippah\Common\Tests\Fixtures\FakeEventListener;
use PHPUnit\Framework\TestCase;

class DispatcherTest extends TestCase
{
    /**
     * @var Dispatcher
     */
    private $dispatcher;

    protected function setUp()
    {
        parent::setUp();

        $this->dispatcher = new Dispatcher();
    }

    public function testShouldCallListener()
    {
        // Arrange
        $listener = new FakeEventListener();

        self::assertFalse($listener->isTriggered());

        $this->dispatcher->register($listener);

        // Act
        $this->dispatcher->dispatch(new FakeEvent());

        // Assert
        self::assertTrue($listener->isTriggered());
    }
}
